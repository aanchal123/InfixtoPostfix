A simple infix to postfix converter program.

Commands to run..
1. >> make
2. >> ./try

Now give any infix expression and it will output the postfix expression.
