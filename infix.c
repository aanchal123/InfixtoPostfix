#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cstack.h"
#include "token.h"
int readline(char *arr, int len) {
	int i = 0;
	int ch;
	while((ch = getchar()) != '\n' && i < len - 1) {
		arr[i] = ch;
		i++;
	}
	arr[i] = '\0';
	return i;
}

int precedence(char op) {
	if(op == '(')
		return 5;
	if(op == '^')
		return 4;
	if(op == '%')
		return 3;
	if(op == '*' || op == '/')
		return 2;
	if(op == '+' || op == '-')
		return 1;
	if(op == ')')
		return 0;
	return -1;
}
char ctop(cstack *cs) {
	char x = cpop(cs);
	cpush(cs, x);
	return x;
}
char *intopost(char *infix) {
	char *result = (char *)malloc(128);
	cstack cs;
	token *t;
	char temp[16];
	int reset = 0, a, b;
	char x;

	strcpy(result, "");
	cinit(&cs);

	while(1) {
		t = getnext (infix, &reset);
		if(t->type == OPERAND) {
			sprintf(temp, "%d", t->number);
			strcat(result, temp);
			strcat(result, " ");			
		}
		else if (t->type == OPERATOR) {
			if(!cempty(&cs)) {
				x = ctop(&cs);	
				a = precedence(x);	
				b = precedence(t->op);	
				while((a >= b) && (x != '(')) {
					x = cpop(&cs);
					if(x != '('){	
						temp[0] = x; temp[1] = ' ';
						temp[2] = '\0';
						strcat(result, temp);
					}
					if(!cempty(&cs)) {
						x = ctop(&cs);	
						a = precedence(x);
						if(x == '(')
							x = cpop(&cs);
					}
					else
						break;
				}
			}
			if(t->op != ')'){
				cpush(&cs, t->op);
			}
		} else if (t->type == END) {
			while(!cempty(&cs)) {
				x = cpop(&cs);
				temp[0] = x; temp[1] = ' ';
				temp[2] = '\0';
				strcat(result, temp);
			}
			return result;
		} else
			return NULL;
	}
}
int main() {
	char str[128];
	int x;
	char *p;
	while((x = readline(str, 128))) {
		p = intopost(str);
		if(p == NULL) {
			fprintf(stderr, "Erorr in expression\n");
		}  else
			{
			fprintf(stdout,"Given Infix Expression is %s\n", str);
			fprintf(stdout, "Postfix expression is %s\n", p);
		}
	}
	return 0;
}
