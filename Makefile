try: infix.o token.o cstack.o
	cc infix.o token.o cstack.o -o try
infix.o: infix.c cstack.h token.h
	cc -c infix.c
token.o: token.c token.h
	cc -c token.c
cstack.o: cstack.c cstack.h
	cc -c cstack.c
clean: rm -f try infix.o token.o cstack.o
